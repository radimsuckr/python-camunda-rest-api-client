# Camunda REST API client

Simple wrapper around Camunda's REST API. Written for version 7.9. [Link to Camunda REST API documentation](https://docs.camunda.org/manual/7.9/reference/rest/).

## Installation

The installation is as simple as `pip install camunda_rest_api_client`.

## Example

```python
from camunda.api import Deployment
from camunda.http import Browser

browser = Browser('http://localhost')
deployment = Deployment(browser)

deployment.get_list()
```

Or you can use [HTTP basic authentication](https://en.wikipedia.org/wiki/Basic_access_authentication).

```python
from camunda.http import Browser

browser = Browser('http://localhost', authenticate=True, username='john', password='doe')
```

## TODO
 - testing
 - implement the rest of the API (?)
 - bit of refactoring
  - exceptions handling
