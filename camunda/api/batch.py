from camunda.http import Endpoint


class Batch(Endpoint):
    def get_list(self, params={}):
        return self.browser.get('batch', params)

    def get_list_count(self, params={}):
        return self.browser.get('batch/count', params)

    def get(self, id):
        return self.browser.get(f'batch/{id}')

    def toggle(self, id, suspended):
        """Activate or suspend batch"""
        return self.browser.put(f'batch/{id}', json={'suspended': suspended})

    def delete(self, id, params={}):
        return self.browser.delete(f'batch/{id}', params)

    def get_statistics(self, params={}):
        return self.browser.get('batch/statistics', params)

    def get_statistics_count(self, params={}):
        return self.browser.get('batch/statistics/count', params)
