from camunda.http import Endpoint


class CaseDefinition(Endpoint):
    def get_list(self, params={}):
        return self.browser.get('case-definition', params)

    def get_list_count(self, params={}):
        return self.browser.get('case-definition/count', params)

    def _get(self, id, key=None, tenant_id=None, type=None):
        if tenant_id and not key:
            raise ValueError('Attribute "key" is missing.')
        types = {None, 'xml', 'diagram'}
        if type not in types:
            raise ValueError(f'Attribute "type" must be one of following values: {types}')

        url = f'case-definition/{id}'
        if key:
            url += f'/key/{key}'
        if tenant_id:
            url += f'/tenant-id/{tenant_id}'
        if type == 'xml':
            url += '/xml'
        if type == 'diagram':
            url += '/diagram'
        return self.browser.get(url)

    def get(self, id):
        return self._get(id)

    def get_by_key(self, id, key):
        return self._get(id, key)

    def get_by_key_and_tenant_id(self, id, key, tenant_id):
        return self._get(id, key, tenant_id)

    def get_xml(self, id):
        return self._get(id, type='xml')

    def get_xml_by_key(self, id, key):
        return self._get(id, key, type='xml')

    def get_xml_by_key_and_tenant_id(self, id, key, tenant_id):
        return self._get(id, key, tenant_id, type='xml')

    def get_diagram(self, id):
        return self._get(id, type='diagram')

    def get_diagram_by_key(self, id, key):
        return self._get(id, key, type='diagram')

    def get_diagram_by_key_and_tenant_id(self, id, key, tenant_id):
        return self._get(id, key, tenant_id, type='diagram')

    def _create_instance(self, variables, business_key, id=None, key=None, tenant_id=None):
        if id:
            url = f'case-definition/{id}/create'
        elif key:
            url = f'case-definition/key/{key}/create'
        elif key and tenant_id:
            url = f'case-definition/key/{key}/tenant-id/{tenant_id}/create'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')

        return self.browser.post(url, json={
            'variables': variables,
            'businessKey': business_key,
        })

    def create_instance(self, id, variables, business_key):
        return self._create_instance(variables, business_key, id=id)

    def create_instance_by_key(self, key, variables, business_key):
        return self._create_instance(variables, business_key, key=key)

    def create_instance_by_key_and_tenant_id(self, key, tenant_id, variables, business_key):
        return self._create_instance(variables, business_key, key=key, tenant_id=tenant_id)

    def _update_history_time_to_live(self, history_time_to_live, id=None, key=None, tenant_id=None):
        if id:
            url = f'case-definition/{id}/history-time-to-live'
        elif key:
            url = f'case-definition/key/{key}/history-time-to-live'
        elif key and tenant_id:
            url = f'case-definition/key/{key}/tenant-id/{tenant_id}/history-time-to-live'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')

        return self.browser.put(url, json={'historyTimeToLive': history_time_to_live})

    def update_history_time_to_live(self, id, history_time_to_live):
        return self._update_history_time_to_live(history_time_to_live, id=id)

    def update_history_time_to_live_by_key(self, key, history_time_to_live):
        return self._update_history_time_to_live(history_time_to_live, key=key)

    def update_history_time_to_live_by_key_and_tenant_id(self, key, tenant_id, history_time_to_live):
        return self._update_history_time_to_live(history_time_to_live, key=key, tenant_id=tenant_id)
