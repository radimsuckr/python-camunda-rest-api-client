from camunda.http import Endpoint


class CaseExecution(Endpoint):
    def get_local_variable(self, id, var_name, params={}):
        return self.browser.get(f'case-execution/{id}/localVariables/{var_name}', params)

    def get_local_variable_binary(self, id, var_name, params={}):
        return self.browser.get(f'case-execution/{id}/localVariables/{var_name}/data', params)

    def get_local_variables(self, id, params):
        return self.browser.get(f'case-execution/{id}/localVariables', params)

    def update_local_variable(self, id, var_name, value, type, value_info):
        return self.browser.put(f'case-execution/{id}/localVariables/{var_name}', json={
            'value': value,
            'type': type,
            'valueInfo': value_info,
        })

    def update_local_variable_binary(self, id, var_name, data, value_type):
        return self.browser.put_form(f'case-execution/{id}/localVariables/{var_name}/data', files={
            'data': data,
            'valueType': value_type,
        })

    def modify_local_variable(self, id, modifications, deletions):
        return self.browser.post(f'case-execution/{id}/localVariables', json={
            'modifications': modifications,
            'deletions': deletions,
        })

    def delete_local_variable(self, id, var_name):
        return self.browser.delete(f'case-execution/{id}/localVariables/{var_name}')

    def get_variable(self, id, var_name, params={}):
        return self.browser.get(f'case-execution/{id}/variables/{var_name}', params)

    def get_variable_binary(self, id, var_name, params={}):
        return self.browser.get(f'case-execution/{id}/variables/{var_name}/data', params)

    def get_variables(self, id, params):
        return self.browser.get(f'case-execution/{id}/variables', params)

    def update_variable(self, id, var_name, value, type, value_info):
        return self.browser.put(f'case-execution/{id}/variables/{var_name}', json={
            'value': value,
            'type': type,
            'valueInfo': value_info,
        })

    def update_variable_binary(self, id, var_name, data, value_type):
        return self.browser.put_form(f'case-execution/{id}/variables/{var_name}/data', files={
            'data': data,
            'valueType': value_type,
        })

    def modify_variable(self, id, modifications, deletions):
        return self.browser.post(f'case-execution/{id}/variables', json={
            'modifications': modifications,
            'deletions': deletions,
        })

    def delete_variable(self, id, var_name):
        return self.browser.delete(f'case-execution/{id}/variables/{var_name}')

    def get_list(self, params={}):
        return self.browser.get('case-execution', params)

    def get_list_count(self, params={}):
        return self.browser.get('case-execution/count', params)

    def get(self, id):
        return self.browser.get(f'case-execution/{id}')

    def post_get_list(self, case_execution_id, case_instance_id, business_key, case_definition_id, case_definition_key,
                      activity_id, required, repeatable, repetition, active, enabled, disabled, tenant_id_in, variables,
                      case_instance_variables, sorting, params={}):
        return self.browser.post('case-execution', params, json={
            'caseExecutionId': case_execution_id,
            'caseInstanceId': case_instance_id,
            'businessKey': business_key,
            'caseDefinitionId': case_definition_id,
            'caseDefinitionKey': case_definition_key,
            'activityId': activity_id,
            'required': required,
            'repeatable': repeatable,
            'repetition': repetition,
            'active': active,
            'enabled': enabled,
            'disabled': disabled,
            'tenantIdIn': tenant_id_in,
            'variables': variables,
            'caseInstanceVariables': case_instance_variables,
            'sorting': sorting,
        })

    def start(self, id, variables, deletions):
        return self.browser.post(f'case-execution/{id}/manual-start', json={
            'variables': variables,
            'deletions': deletions,
        })

    def complete(self, id, variables, deletions):
        return self.browser.post(f'case-execution/{id}/complete', json={
            'variables': variables,
            'deletions': deletions,
        })

    def disable(self, id, variables, deletions):
        return self.browser.post(f'case-execution/{id}/disable', json={
            'variables': variables,
            'deletions': deletions,
        })

    def reenable(self, id, variables, deletions):
        return self.browser.post(f'case-execution/{id}/reenable', json={
            'variables': variables,
            'deletions': deletions,
        })

    def terminate(self, id, variables, deletions):
        return self.browser.post(f'case-execution/{id}/terminate', json={
            'variables': variables,
            'deletions': deletions,
        })
