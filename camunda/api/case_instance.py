from camunda.http import Endpoint


class CaseInstance(Endpoint):
    def get_variable(self, id, var_name, params={}):
        return self.browser.get(f'case-instance/{id}/variables/{var_name}', params)

    def get_variable_binary(self, id, var_name, params={}):
        return self.browser.get(f'case-instance/{id}/variables/{var_name}/data', params)

    def get_variables(self, id, params):
        return self.browser.get(f'case-instance/{id}/variables', params)

    def update_variable(self, id, var_name, value, type, value_info):
        return self.browser.put(f'case-instance/{id}/variables/{var_name}', json={
            'value': value,
            'type': type,
            'valueInfo': value_info,
        })

    def post_variable_binary(self, id, var_name, data, value_type):
        return self.browser.post_form(f'case-instance/{id}/variables/{var_name}', files={
            'data': data,
            'valueType': value_type,
        })

    def modify_variable(self, id, modifications, deletions):
        return self.browser.post(f'case-instance/{id}/variables', json={
            'modifications': modifications,
            'deletions': deletions,
        })

    def delete_variable(self, id, var_name):
        return self.browser.delete(f'case-instance/{id}/variables/{var_name}')

    def get_list(self, params={}):
        return self.browser.get('case-instance', params)

    def get_list_count(self, params={}):
        return self.browser.get('case-instance/count', params)

    def get(self, id):
        return self.browser.get(f'case-instance/{id}')

    def post_get_list(self, case_instance_id, business_key, case_definition_id, case_definition_key, deployment_id,
                      super_process_instance, sub_process_instance, super_case_instance, sub_case_instance, active,
                      completed, tenant_id_in, without_tenant_id, variables, sorting, params={}):
        return self.browser.post('case-instance', params, json={
            'caseInstanceId': case_instance_id,
            'businessKey': business_key,
            'caseDefinitionId': case_definition_id,
            'caseDefinitionKey': case_definition_key,
            'deploymentId': deployment_id,
            'superProcessInstance': super_process_instance,
            'subProcessInstance': sub_process_instance,
            'superCaseInstance': super_case_instance,
            'subCaseInstance': sub_case_instance,
            'active': active,
            'completed': completed,
            'tenantIdIn': tenant_id_in,
            'withoutTenantId': without_tenant_id,
            'variables': variables,
            'sorting': sorting,
        })

    def post_get_list_count(self, case_instance_id, business_key, case_definition_id, case_definition_key,
                            deployment_id, super_process_instance, sub_process_instance, super_case_instance,
                            sub_case_instance, active, completed, tenant_id_in, without_tenant_id, variables, sort_by,
                            sort_order, params={}):
        return self.browser.post('case-instance', params, json={
            'caseInstanceId': case_instance_id,
            'businessKey': business_key,
            'caseDefinitionId': case_definition_id,
            'caseDefinitionKey': case_definition_key,
            'deploymentId': deployment_id,
            'superProcessInstance': super_process_instance,
            'subProcessInstance': sub_process_instance,
            'superCaseInstance': super_case_instance,
            'subCaseInstance': sub_case_instance,
            'active': active,
            'completed': completed,
            'tenantIdIn': tenant_id_in,
            'withoutTenantId': without_tenant_id,
            'variables': variables,
            'sortBy': sort_by,
            'sortOrder': sort_order,
        })

    def start(self, id, variables, deletions):
        return self.browser.post(f'case-instance/{id}/manual-start', json={
            'variables': variables,
            'deletions': deletions,
        })

    def complete(self, id, variables, deletions):
        return self.browser.post(f'case-instance/{id}/complete', json={
            'variables': variables,
            'deletions': deletions,
        })

    def close(self, id, variables, deletions):
        return self.browser.post(f'case-instance/{id}/close', json={
            'variables': variables,
            'deletions': deletions,
        })

    def terminate(self, id, variables, deletions):
        return self.browser.post(f'case-instance/{id}/terminate', json={
            'variables': variables,
            'deletions': deletions,
        })
