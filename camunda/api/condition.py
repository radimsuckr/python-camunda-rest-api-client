from camunda.http import Endpoint


class Condition(Endpoint):
    def evaluate(self, variables, business_key, tenant_id, without_tenant_id, process_definition_id):
        return self.browser.post('condition', json={
            'variables': variables,
            'businessKey': business_key,
            'tenantId': tenant_id,
            'withoutTenantId': without_tenant_id,
            'processDefinitionId': process_definition_id,
        })
