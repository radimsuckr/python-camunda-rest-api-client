from camunda.http import Endpoint


class DecisionDefinition(Endpoint):
    def get_list(self, params={}):
        return self.browser.get('decision-definition', params)

    def get_list_count(self, params={}):
        return self.browser.get('decision-definition/count', params)

    def _get(self, id=None, key=None, tenant_id=None, xml=False, diagram=False):
        if id:
            url = f'decision-definition/{id}'
        elif key:
            url = f'decision-definition/key/{key}'
        elif key and tenant_id:
            url = f'decision-definition/key/{key}/tenant-id/{tenant_id}'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')

        if xml and diagram:
            raise ValueError('Provide only one from "xml" or "diagram" parameters')

        if xml:
            url += '/xml'
        elif diagram:
            url += '/diagram'

        return self.browser.get(url)

    def get(self, id):
        return self._get(id)

    def get_by_key(self, key):
        return self._get(key)

    def get_by_key_and_tenant_id(self, key, tenant_id):
        return self._get(key, tenant_id)

    def _evaluate(self, variables, id=None, key=None, tenant_id=None):
        if id:
            url = f'decision-definition/{id}/evaluate'
        elif key:
            url = f'decision-definition/key/{key}/evaluate'
        elif key and tenant_id:
            url = f'decision-definition/key/{key}/tenant-id/{tenant_id}/evaluate'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')

        return self.browser.post(url, json={'variables': variables})

    def evaluate_(self, id, variables):
        return self._evaluate(variables, id)

    def evaluate_by_key(self, key, variables):
        return self._evaluate(variables, key)

    def evaluate_by_key_and_tenant_id(self, key, tenant_id, variables):
        return self._evaluate(variables, key, tenant_id)

    def _update_history_time_to_live(self, history_time_to_live, id=None, key=None, tenant_id=None):
        if id:
            url = f'decision-definition/{id}/history-time-to-live'
        elif key:
            url = f'decision-definition/key/{key}/history-time-to-live'
        elif key and tenant_id:
            url = f'decision-definition/key/{key}/tenant-id/{tenant_id}/history-time-to-live'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')

        return self.browser.put(url, json={'historyTimeToLive': history_time_to_live})

    def update_history_time_to_live(self, id, history_time_to_live):
        return self._update_history_time_to_live(history_time_to_live, id)

    def update_history_time_to_live_by_key(self, key, history_time_to_live):
        return self._update_history_time_to_live(history_time_to_live, key)

    def update_history_time_to_live_by_key_and_tenant_id(self, key, tenant_id, history_time_to_live):
        return self._update_history_time_to_live(history_time_to_live, key, tenant_id)
