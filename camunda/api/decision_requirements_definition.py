from camunda.http import Endpoint


class DecisionRequirementsDefinition(Endpoint):
    def get_list(self, params={}):
        return self.browser.get('decision-definition', params)

    def get_list_count(self, params={}):
        return self.browser.get('decision-definition/count', params)

    def _get(self, id=None, key=None, tenant_id=None, xml=False, diagram=False):
        if id:
            url = f'decision-definition/{id}'
        elif key:
            url = f'decision-definition/key/{key}'
        elif key and tenant_id:
            url = f'decision-definition/key/{key}/tenant-id/{tenant_id}'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')

        if xml and diagram:
            raise ValueError('Provide only one from "xml" or "diagram" parameters')

        if xml:
            url += '/xml'
        elif diagram:
            url += '/diagram'

        return self.browser.get(url)

    def get(self, id):
        return self._get(id)

    def get_by_key(self, key):
        return self._get(key)

    def get_by_key_and_tenant_id(self, key, tenant_id):
        return self._get(key, tenant_id)
