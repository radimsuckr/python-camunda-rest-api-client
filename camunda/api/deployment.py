from camunda.http import Endpoint


class Deployment(Endpoint):
    def get_list(self, params={}):
        return self.browser.get('deployment', params)

    def get_list_count(self, params={}):
        return self.browser.get('deployment/count', params)

    def get(self, id):
        return self.browser.get(f'deployment/{id}')

    def post(self, deployment_name, enable_duplicate_filtering, deploy_changed_only, deployment_source, tenant_id,
             binary_data):
        return self.browser.post_form('deployment/create', files={
            'deployment-name': deployment_name,
            'enable-duplicate-filtering': enable_duplicate_filtering,
            'deploy-changed-only': deploy_changed_only,
            'deployment-source': deployment_source,
            'tenant-id': tenant_id,
            '*': binary_data,
        })

    def redeploy(self, id, resource_ids, resource_names, source):
        return self.browser.post(f'deployment/{id}/redeploy', json={
            'resourceIds': resource_ids,
            'resourceNames': resource_names,
            'source': source,
        })

    def get_resources(self, id):
        return self.browser.get(f'deployment/{id}/resources')

    def get_resource(self, id, resource_id):
        return self.browser.get(f'deployment/{id}/resources/{resource_id}')

    def get_resource_binary(self, id, resource_id):
        return self.browser.get(f'deployment/{id}/resources/{resource_id}/data')

    def delete(self, id, cascade, skip_custom_listeners, skip_io_mappings):
        return self.browser.delete(f'deployment/{id}', params={
            'cascade': cascade,
            'skipCustomListeners': skip_custom_listeners,
            'skip_io_mappings': skip_io_mappings,
        })
