from camunda.http import Endpoint


class Engine(Endpoint):
    def get_list(self):
        return self.browser.get('engine')
