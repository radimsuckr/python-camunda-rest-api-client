from camunda.http import Endpoint


class Execution(Endpoint):
    def get_local_variable(self, id, var_name, params={}):
        return self.browser.get(f'execution/{id}/localVariables/{var_name}', params)

    def get_local_variable_binary(self, id, var_name, params={}):
        return self.browser.get(f'execution/{id}/localVariables/{var_name}/data', params)

    def get_local_variables(self, id, params):
        return self.browser.get(f'execution/{id}/localVariables', params)

    def update_local_variable(self, id, var_name, value, type, value_info):
        return self.browser.put(f'execution/{id}/localVariables/{var_name}', json={
            'value': value,
            'type': type,
            'valueInfo': value_info,
        })

    def post_local_variable_binary(self, id, var_name, data, value_type):
        return self.browser.put_form(f'execution/{id}/localVariables/{var_name}/data', files={
            'data': data,
            'valueType': value_type,
        })

    def modify_local_variable(self, id, modifications, deletions):
        return self.browser.post(f'execution/{id}/localVariables', json={
            'modifications': modifications,
            'deletions': deletions,
        })

    def delete_local_variable(self, id, var_name):
        return self.browser.delete(f'execution/{id}/localVariables/{var_name}')

    def get_message_event_subscription(self, id, message_name):
        return self.browser.get(f'execution/{id}/messageSubscriptions/{message_name}')

    def trigger_message_event_subscription(self, id, message_name, variables):
        return self.browser.post(f'execution/{id}/messageSubscriptions/{message_name}', json={
            'variables': variables,
        })

    def get_list(self, params={}):
        return self.browser.get('execution', params)

    def get_list_count(self, params={}):
        return self.browser.get('execution/count', params)

    def get(self, id):
        return self.browser.get(f'execution/{id}')

    def create_incident(self, id, incident_type, configuration, message):
        return self.browser.post(f'execution/{id}/create-incident', json={
            'incidentType': incident_type,
            'configuration': configuration,
            'message': message,
        })

    def post_get_list(self, business_key, process_definition_id, process_definition_key, process_instance_id,
                      activity_id, signal_event_subscription_name, message_event_subscription_name, active, suspended,
                      incident_id, incident_type, incident_message, incident_message_like, tenant_id_in, variables,
                      process_variables, sorting, params={}):
        return self.browser.post('execution', params=params, json={
            'businessKey': business_key,
            'processDefinitionId': process_definition_id,
            'processDefinitionKey': process_definition_key,
            'processInstanceId': process_instance_id,
            'activityId': activity_id,
            'signalEventSubscriptionName': signal_event_subscription_name,
            'messageEventSubscriptionName': message_event_subscription_name,
            'active': active,
            'suspended': suspended,
            'incidentId': incident_id,
            'incidentType': incident_type,
            'incidentMessage': incident_message,
            'incidentMessageLike': incident_message_like,
            'tenantIdIn': tenant_id_in,
            'variables': variables,
            'processVariables': process_variables,
            'sorting': sorting,
        })

    def post_get_list_count(self, business_key, process_definition_id, process_definition_key, process_instance_id,
                            activity_id, signal_event_subscription_name, message_event_subscription_name, active,
                            suspended, incident_id, incident_type, incident_message, incident_message_like,
                            tenant_id_in, variables, process_variables):
        return self.browser.post('execution/count', json={
            'businessKey': business_key,
            'processDefinitionId': process_definition_id,
            'processDefinitionKey': process_definition_key,
            'processInstanceId': process_instance_id,
            'activityId': activity_id,
            'signalEventSubscriptionName': signal_event_subscription_name,
            'messageEventSubscriptionName': message_event_subscription_name,
            'active': active,
            'suspended': suspended,
            'incidentId': incident_id,
            'incidentType': incident_type,
            'incidentMessage': incident_message,
            'incidentMessageLike': incident_message_like,
            'tenantIdIn': tenant_id_in,
            'variables': variables,
            'processVariables': process_variables,
        })

    def trigger(self, id, variables):
        return self.browser.post(f'execution/{id}/signal', json={
            'variables': variables,
        })
