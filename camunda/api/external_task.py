from camunda.http import Endpoint


class ExternalTask(Endpoint):
    def get(self, id):
        return self.browser.get(f'external-task/{id}')

    def get_list(self, params={}):
        return self.browser.get('external-task', params)

    def get_list_count(self, params={}):
        return self.browser.get('external-task/count', params)

    def post_get_list(self, external_task_id, topic_name, worker_id, locked, not_locked, with_retries_left,
                      no_retries_left, lock_expiration_after, lock_expiration_before, activity_id, activity_id_in,
                      execution_id, process_instance_id, process_definition_id, tenant_id_in, active, suspended,
                      priority_higher_than_or_equals, priority_lower_than_or_equals, sorting, params={}):
        return self.browser.post('external-task', params=params, json={
            'externalTaskId': external_task_id,
            'topicName': topic_name,
            'workerId': worker_id,
            'locked': locked,
            'notLocked': not_locked,
            'withRetriesLeft': with_retries_left,
            'noRetriesLeft': no_retries_left,
            'lockExpirationAfter': lock_expiration_after,
            'lockExpirationBefore': lock_expiration_before,
            'activityId': activity_id,
            'activityIdIn': activity_id_in,
            'executionId': execution_id,
            'processInstanceId': process_instance_id,
            'processDefinitionId': process_definition_id,
            'tenantIdIn': tenant_id_in,
            'active': active,
            'suspended': suspended,
            'priorityHigherThanOrEquals': priority_higher_than_or_equals,
            'priorityLowerThanOrEquals': priority_lower_than_or_equals,
            'sorting': sorting,
        })

    def post_get_list_count(self, external_task_id, topic_name, worker_id, locked, not_locked, with_retries_left,
                            no_retries_left, lock_expiration_after, lock_expiration_before, activity_id, activity_id_in,
                            execution_id, process_instance_id, process_definition_id, tenant_id_in, active, suspended,
                            priority_higher_than_or_equals, priority_lower_than_or_equals):
        return self.browser.post('external-task', json={
            'externalTaskId': external_task_id,
            'topicName': topic_name,
            'workerId': worker_id,
            'locked': locked,
            'notLocked': not_locked,
            'withRetriesLeft': with_retries_left,
            'noRetriesLeft': no_retries_left,
            'lockExpirationAfter': lock_expiration_after,
            'lockExpirationBefore': lock_expiration_before,
            'activityId': activity_id,
            'activityIdIn': activity_id_in,
            'executionId': execution_id,
            'processInstanceId': process_instance_id,
            'processDefinitionId': process_definition_id,
            'tenantIdIn': tenant_id_in,
            'active': active,
            'suspended': suspended,
            'priorityHigherThanOrEquals': priority_higher_than_or_equals,
            'priorityLowerThanOrEquals': priority_lower_than_or_equals,
        })

    def fetch_and_lock(self, worker_id, max_tasks, user_priority, async_response_timeout, topics):
        return self.browser.post('external-task/fetchAndLock', json={
            'workerId': worker_id,
            'maxTasks': max_tasks,
            'userPriority': user_priority,
            'asyncResponseTimeout': async_response_timeout,
            'topics': topics,
        })

    def complete(self, id, worker_id, variables, local_variables):
        return self.browser.post(f'external-task/{id}/complete', json={
            'workerId': worker_id,
            'variables': variables,
            'localVariables': local_variables,
        })

    def handle_bpmn_error(self, id, worker_id, error_code):
        return self.browser.post(f'external-task/{id}/bpmnError', json={
            'workerId': worker_id,
            'errorCode': error_code,
        })

    def handle_failure(self, id, worker_id, error_message, error_details, retries, retry_timeout):
        return self.browser.post(f'external-task/{id}/failure', json={
            'workerId': worker_id,
            'errorMessage': error_message,
            'errorDetails': error_details,
            'retries': retries,
            'retryTimeout': retry_timeout,
        })

    def unlock(self, id):
        return self.browser.post(f'external-task/{id}/unlock')

    def extend_lock(self, id, new_duration, worker_id):
        return self.browser.post(f'external-task/{id}/extendLock', json={
            'newDuration': new_duration,
            'workerId': worker_id,
        })

    def set_priority(self, id, priority):
        return self.browser.put(f'external-task/{id}/priority', json={
            'priority': priority,
        })

    def set_retries(self, id, retries):
        return self.browser.put(f'external-task/{id}/retries', json={
            'retries': retries,
        })

    def set_retries_async(self, id, retries, external_task_ids, process_instance_ids, external_task_query,
                          process_instance_query, historic_process_instance_query):
        return self.browser.post(f'external-task/{id}/retries-async', json={
            'retries': retries,
            'externalTaskIds': external_task_ids,
            'processInstanceIds': process_instance_ids,
            'externalTaskQuery': external_task_query,
            'processInstanceQuery': process_instance_query,
            'historicProcessInstanceQuery': historic_process_instance_query,
        })

    def set_retries_sync(self, id, retries, external_task_ids, process_instance_ids, external_task_query,
                         process_instance_query, historic_process_instance_query):
        return self.browser.put(f'external-task/{id}/retries-sync', json={
            'retries': retries,
            'externalTaskIds': external_task_ids,
            'processInstanceIds': process_instance_ids,
            'externalTaskQuery': external_task_query,
            'processInstanceQuery': process_instance_query,
            'historicProcessInstanceQuery': historic_process_instance_query,
        })
