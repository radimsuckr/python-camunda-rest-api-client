from camunda.http import Endpoint


class Identity(Endpoint):
    def get_groups(self, params={}):
        return self.browser.get('identity/groups', params)

    def verify(self, username, password):
        return self.browser.post('identity/verify', json={
            'username': username,
            'password': password,
        })
