from camunda.http import Endpoint


class Incident(Endpoint):
    def get(self, id):
        return self.browser.get(f'incident/{id}')

    def get_list(self, params={}):
        return self.browser.get('incident', params)

    def get_list_count(self, params={}):
        return self.browser.get('incident/count', params)

    def resolve(self, id):
        return self.browser.delete(f'incident/{id}')
