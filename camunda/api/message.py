from camunda.http import Endpoint


class Message(Endpoint):
    def correlate(self, message_name, business_key, tenant_id_in, without_tenant_id, process_instance_id,
                  correlation_keys, local_correlation_keys, process_variables, all, result_enabled):
        return self.browser.post('message', json={
            'messageName': message_name,
            'businessKey': business_key,
            'tenantIdIn': tenant_id_in,
            'withoutTenantId': without_tenant_id,
            'processInstanceId': process_instance_id,
            'correlationKeys': correlation_keys,
            'localCorrelationKeys': local_correlation_keys,
            'processVariables': process_variables,
            'all': all,
            'resultEnabled': result_enabled,
        })
