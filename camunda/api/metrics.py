from camunda.http import Endpoint


class Metrics(Endpoint):
    def get_list(self, params={}):
        return self.browser.get('metrics', params)

    def get_sum(self, metrics_name, params={}):
        return self.browser.get(f'metrics/{metrics_name}/sum', params)
