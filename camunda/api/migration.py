from camunda.http import Endpoint


class Migration(Endpoint):
    def generate(self, source_process_definition_id, target_process_definition_id, update_event_triggers):
        return self.browser.post('migration/generate', json={
            'sourceProcessDefinitionId': source_process_definition_id,
            'targetProcessDefinitionId': target_process_definition_id,
            'updateEventTriggers': update_event_triggers,
        })

    def validate(self, source_process_definition_id, target_process_definition_id, instructions):
        return self.browser.post('migration/validate', json={
            'sourceProcessDefinitionId': source_process_definition_id,
            'targetProcessDefinitionId': target_process_definition_id,
            'instructions': instructions,
        })

    def execute(self, migration_plan, process_instance_ids, process_instance_query, skip_custom_listeners,
                skip_io_mappings):
        return self.browser.post('migration/execute', json={
            'migrationPlan': migration_plan,
            'processInstanceIds': process_instance_ids,
            'processInstanceQuery': process_instance_query,
            'skipCustomListeners': skip_custom_listeners,
            'skipIoMappings': skip_io_mappings,
        })

    def execute_async(self, migration_plan, process_instance_ids, process_instance_query, skip_custom_listeners,
                      skip_io_mappings):
        return self.browser.post('migration/executeAsync', json={
            'migrationPlan': migration_plan,
            'processInstanceIds': process_instance_ids,
            'processInstanceQuery': process_instance_query,
            'skipCustomListeners': skip_custom_listeners,
            'skipIoMappings': skip_io_mappings,
        })
