from camunda.http import Endpoint


class Modification(Endpoint):
    def execute(self, process_definition_id, skip_custom_listeners, skip_io_mappings, process_instance_ids,
                process_instance_query, instructions):
        return self.browser.post('modification/execute', json={
            'processDefinitionId': process_definition_id,
            'skipCustomListeners': skip_custom_listeners,
            'skipIoMappings': skip_io_mappings,
            'processInstanceIds': process_instance_ids,
            'processInstanceQuery': process_instance_query,
            'instructions': instructions,
        })

    def execute_async(self, process_definition_id, skip_custom_listeners, skip_io_mappings, process_instance_ids,
                      process_instance_query, instructions):
        return self.browser.post('modification/executeAsync', json={
            'processDefinitionId': process_definition_id,
            'skipCustomListeners': skip_custom_listeners,
            'skipIoMappings': skip_io_mappings,
            'processInstanceIds': process_instance_ids,
            'processInstanceQuery': process_instance_query,
            'instructions': instructions,
        })
