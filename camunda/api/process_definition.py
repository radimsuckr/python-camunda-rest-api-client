from camunda.http import Endpoint


class ProcessDefinition(Endpoint):
    # Does not implement everything

    def restart_instance(self, id, process_instance_ids, historic_process_instance_query, skip_custom_listeners,
                         skip_io_mappings, initial_variables, without_business_key, instructions):
        return self.browser.post(f'process-definition/{id}/restart', json={
            'processInstanceIds': process_instance_ids,
            'historicProcessInstanceQuery': historic_process_instance_query,
            'skipCustomListeners': skip_custom_listeners,
            'skipIoMappings': skip_io_mappings,
            'initialVariables': initial_variables,
            'withoutBusinessKey': without_business_key,
            'instructions': instructions,
        })

    def _start_instance(self, variables, business_key, case_instance_id, start_instructions, skip_custom_listeners,
                        skip_io_mappings, with_variables_in_return, id=None, key=None, tenant_id=None):
        if id:
            url = f'process-definition/{id}'
        elif key:
            url = f'process-definition/key/{key}'
        elif key and tenant_id:
            url = f'process-definition/key/{key}/tenant-id/{tenant_id}'
        else:
            raise ValueError('Either provide "id", "key" or both "key" and "tenant_id"')
        url += '/start'

        return self.browser.post(url, json={
            'variables': variables,
            'businessKey': business_key,
            'caseInstanceId': case_instance_id,
            'startInstructions': start_instructions,
            'skipCustomListeners': skip_custom_listeners,
            'skip_io_mappings': skip_io_mappings,
            'withVariablesInReturn': with_variables_in_return,
        })

    def start_instance(self, id, variables, business_key, case_instance_id, start_instructions, skip_custom_listeners,
                       skip_io_mappings, with_variables_in_return):
        return self._start_instance(variables, business_key, case_instance_id, start_instructions,
                                    skip_custom_listeners, skip_io_mappings, with_variables_in_return, id=id)

    def start_instance_by_key(self, key, variables, business_key, case_instance_id, start_instructions,
                              skip_custom_listeners, skip_io_mappings, with_variables_in_return):
        return self._start_instance(variables, business_key, case_instance_id, start_instructions,
                                    skip_custom_listeners, skip_io_mappings, with_variables_in_return, key=key)

    def start_instance_by_key_and_tenant_id(self, key, tenant_id, variables, business_key, case_instance_id,
                                            start_instructions, skip_custom_listeners, skip_io_mappings,
                                            with_variables_in_return):
        return self._start_instance(variables, business_key, case_instance_id, start_instructions,
                                    skip_custom_listeners, skip_io_mappings, with_variables_in_return, key=key,
                                    tenant_id=tenant_id)

    def delete(self, id, params={}):
        return self.browser.delete(f'process-definition/{id}', params=params)
