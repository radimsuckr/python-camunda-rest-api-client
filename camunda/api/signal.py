from camunda.http import Endpoint


class Signal(Endpoint):
    def throw(self, name, execution_id, variables, tenant_id, without_tenant_id):
        return self.browser.post('signal', json={
            'name': name,
            'executionId': execution_id,
            'variables': variables,
            'tenantId': tenant_id,
            'withoutTenantId': without_tenant_id,
        })
