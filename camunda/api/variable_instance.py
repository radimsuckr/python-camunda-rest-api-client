from camunda.http import Endpoint


class VariableInstance(Endpoint):
    def get_binary(self, id):
        return self.browser.get(f'variable-instance/{id}/data')

    def get(self, id, params={}):
        return self.browser.get(f'variable-instance/{id}', params)

    def get_list(self, params={}):
        return self.browser.get('variable-instance', params)

    def get_list_count(self, params={}):
        return self.browser.get('variable-instance/count', params)

    def post_get_list(self, variable_name, variable_name_like, process_instance_id_in, execution_id_in,
                      case_instance_id_in, case_execution_id_in, task_id_in, activity_instance_id_in, tenant_id_in,
                      variable_values, sorting, params={}):
        return self.browser.post('variable-instance', params=params, json={
            'variableName': variable_name,
            'variableNameLike': variable_name_like,
            'processInstanceIdIn': process_instance_id_in,
            'executionIdIn': execution_id_in,
            'caseInstanceIdIn': case_instance_id_in,
            'caseExecutionIdIn': case_execution_id_in,
            'taskIdIn': task_id_in,
            'activityInstanceIdIn': activity_instance_id_in,
            'tenantIdIn': tenant_id_in,
            'variableValues': variable_values,
            'sorting': sorting,
        })

    def post_get_list_count(self, variable_name, variable_name_like, process_instance_id_in, execution_id_in,
                            case_instance_id_in, case_execution_id_in, task_id_in, activity_instance_id_in,
                            tenant_id_in, variable_values, sort_by, sort_order, params={}):
        return self.browser.post('variable-instance/count', params=params, json={
            'variableName': variable_name,
            'variableNameLike': variable_name_like,
            'processInstanceIdIn': process_instance_id_in,
            'executionIdIn': execution_id_in,
            'caseInstanceIdIn': case_instance_id_in,
            'caseExecutionIdIn': case_execution_id_in,
            'taskIdIn': task_id_in,
            'activityInstanceIdIn': activity_instance_id_in,
            'tenantIdIn': tenant_id_in,
            'variableValues': variable_values,
            'sortBy': sort_by,
            'sortOrder': sort_order,
        })
