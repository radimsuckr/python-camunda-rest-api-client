from http import HTTPStatus as HS


class CamundaAPIError(Exception):
    ERRORS = {
        HS.BAD_REQUEST.value: HS.BAD_REQUEST.phrase,
        HS.FORBIDDEN.value: HS.FORBIDDEN.phrase,
        HS.NOT_FOUND.value: HS.NOT_FOUND.phrase,
        HS.INTERNAL_SERVER_ERROR.value: HS.INTERNAL_SERVER_ERROR.phrase,
    }

    def __init__(self, response):
        self.response = response

    def __str__(self):
        error = self.ERRORS.get(self.response.status_code, 'Unknown error')
        return f'{self.response.status_code}: {error}\n\n{self.response.json}'
