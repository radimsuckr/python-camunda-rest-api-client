import json
from dataclasses import dataclass
from http import HTTPStatus as HS

import requests

from camunda.exceptions import CamundaAPIError


@dataclass
class Response:
    content: str
    headers: dict
    status_code: int

    @property
    def json(self):
        return json.loads(self.content)


class Browser:
    OK_STATUS_CODES = {HS.OK, HS.NO_CONTENT}

    def __init__(self, address, authenticate=False, username='', password=''):
        self._address = address if address.endswith('/') else f'{address}/'
        self._auth = requests.auth.HTTPBasicAuth(username, password) if authenticate else None

    def _get_url(self, path):
        return f'{self._address}engine-rest/{path}'

    def _process_response(self, response):
        result = Response(content=response.content, headers=response.headers, status_code=response.status_code)

        if result.status_code not in self.OK_STATUS_CODES:
            raise CamundaAPIError(result)
        return result

    def _call(self, method, endpoint, *args, **kwargs):
        if self._auth:
            kwargs['auth'] = self._auth
        return requests.request(method, self._get_url(endpoint), *args, **kwargs)

    def get(self, endpoint, params={}):
        return self._process_response(self._call('GET', endpoint, params=params))

    def post(self, endpoint, params={}, json={}):
        return self._process_response(self._call('POST', endpoint, params=params, json=json))

    def post_form(self, endpoint, params={}, files={}):
        return self._process_response(self._call('POST', endpoint, params=params, files=files))

    def delete(self, endpoint, params={}):
        return self._process_response(self._call('DELETE', endpoint, params=params))

    def put(self, endpoint, params={}, json={}):
        return self._process_response(self._call('PUT', endpoint, params=params, json=json))

    def put_form(self, endpoint, params={}, files={}):
        return self._process_response(self._call('PUT', endpoint, params=params, files=files))


class Endpoint:
    def __init__(self, browser):
        self.browser = browser
