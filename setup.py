import setuptools

with open('README.md', 'r') as file:
    long_description = file.read()

setuptools.setup(
    name='camunda_rest_api_client',
    version='0.0.1',
    author='Radim Sückr',
    author_email='contact@radimsuckr.cz',
    description='Wrapper around Camunda REST API',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/radimsuckr/python-camunda-rest-client',
    packages=setuptools.find_packages(exclude=('tests',)),
    install_requires=(
        'requests==2.19.1',
    ),
    python_requires='~=3.7',
    classifiers=(
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    )
)
