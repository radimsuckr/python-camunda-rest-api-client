# TODO: Figure out how to properly prepare test data inside Camunda
# TODO: Unittest module "data providers" or "fixtures" per test, not only for all
import unittest
from http import HTTPStatus as HS

from camunda import client
from camunda.exceptions import APICallError

__all__ = (
    'CamundaBatchTestCase',
    'CamundaDeploymentTestCase',
)


class APIMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.api = client('http://localhost')


class CamundaBatchTestCase(APIMixin, unittest.TestCase):
    # TODO: Testing data

    def test_get_batches(self):
        response = self.api.get_batches()
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(len(response.json), 0)

    def test_get_batches_count(self):
        response = self.api.get_batches_count()
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(response.json['count'], 0)


class CamundaDeploymentTestCase(APIMixin, unittest.TestCase):
    def setUp(self):
        deployments = self.api.get_deployments()
        for item in deployments.json:
            try:
                self.api.delete_deployment(item['id'], True, False, False)
            except APICallError:
                pass

    def test_get_deployments(self):
        response = self.api.get_deployments()
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(len(response.json), 2)

    def test_get_deployments_count(self):
        response = self.api.get_deployments_count()
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(response.json['count'], 2)

    def test_get_deployment(self):
        deployments = self.api.get_deployments()
        deployment = deployments.json[0]
        response = self.api.get_deployment(deployment['id'])
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(response.json['id'], deployment['id'])

    def test_post_deployment(self):
        args = {
            'deployment_name': 'test_post_deployment',
            'enable_duplicate_filtering': True,
            'deploy_changed_only': False,
            'deployment_source': 'cockpit',
            'tenant_id': None,
            'binary_data': b'',
        }
        response = self.api.post_deployment(**args)
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(response.json['name'], args['deployment_name'])
        self.assertEqual(response.json['source'], args['deployment_source'])
        self.assertEqual(response.json['tenantId'], args['tenant_id'])

    def test_redeploy_development(self):
        deployment = self.api.get_deployments().json[0]
        args = {
            'id': deployment['id'],
            'resource_ids': [],
            'resource_names': [],
            'source': 'cockpit',
        }
        response = self.api.redeploy_deployment(**args)
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(response.json['source'], args['source'])

    def test_get_deployment_resources(self):
        deployment = self.api.get_deployments().json[0]
        args = {
            'id': deployment['id'],
        }
        response = self.api.get_deployment_resources(**args)
        self.assertEqual(response.status_code, HS.OK)
        for resource in response.json:
            self.assertEqual(resource['deploymentId'], args['id'])

    def test_get_deployment_resource(self):
        deployment = self.api.get_deployments().json[0]
        resource = self.api.get_deployment_resources(deployment['id']).json[0]
        args = {
            'id': deployment['id'],
            'resource_id': resource['id'],
        }
        response = self.api.get_deployment_resource(**args)
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(response.json['id'], resource['id'])
        self.assertEqual(response.json['deploymentId'], deployment['id'])

    def test_get_deployment_resource_binary(self):
        deployment = self.api.get_deployments().json[0]
        resource = self.api.get_deployment_resources(deployment['id']).json[0]
        args = {
            'id': deployment['id'],
            'resource_id': resource['id'],
        }
        response = self.api.get_deployment_resource_binary(**args)
        self.assertEqual(response.status_code, HS.OK)
        self.assertEqual(type(response.content), bytes)

    def delete_deployment(self):
        deployment = self.api.get_deployments(name='test_post_deployment').json[0]
        args = {
            'id': deployment['id'],
            'cascade': True,
            'skip_custom_listeners': False,
            'skip_io_mappings': False,
        }
        response = self.api.delete_deployment(**args)
        self.assertEqual(response.status_code, HS.NO_CONTENT)
        # TODO: Proper data manipulation
        deployments = self.api.get_deployments(name='test_post_deployment')
        self.assertEqual(len(deployments.json), 2)
